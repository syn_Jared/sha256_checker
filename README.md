This is just a simple hash computation script that will take a filename as a 
cli argument and compute the sha256 hash of it. 

Written in python, environment set for linux systems and can easily be modified 
to compute whatever hash you want by swapping out the hashlib of your choice.

