#!/usr/bin/env python       
# line above is for linux systems as preferred method of invoking interpreter for executable scripts.

#syn_jared@gmail.com
#@syn_jared
# 12/07/15 
# file in misc scripts folder for use later. 

# import hash and system modules here
import hashlib, sys

def check_hash(filename):
   #choose your hash library here (md5, sha1, sha256, sha512, etc)
   c_hash = hashlib.sha256()

   # open our file for reading in binary mode (rb)
   with open(filename,'rb') as file:       
       chunk = 0
       print("Working - Please wait.")
       while chunk != b'':
           chunk = file.read(4096)  #decided on a larger chunk, so it would scan faster, change as appropriate for your machine
           c_hash.update(chunk)
   return c_hash.hexdigest()

# take argument from command line and use it as filename to check the hash
result = check_hash(sys.argv[1])

# print result back to std_io (screen)
print(result)